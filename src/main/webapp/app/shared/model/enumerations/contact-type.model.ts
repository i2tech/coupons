export const enum ContactType {
  DEVELOPER = 'DEVELOPER',
  CUSTOMER = 'CUSTOMER',
  ADMIN = 'ADMIN'
}
