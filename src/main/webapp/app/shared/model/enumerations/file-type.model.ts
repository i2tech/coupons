export const enum FileType {
  PHOTO = 'PHOTO',
  LOGO = 'LOGO',
  DOCUMENT = 'DOCUMENT'
}
