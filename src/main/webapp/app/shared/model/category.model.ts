import { IProduct } from 'app/shared/model/product.model';

export interface ICategory {
  id?: number;
  name?: string;
  categoryId?: number;
  categories?: IProduct[];
}

export class Category implements ICategory {
  constructor(public id?: number, public name?: string, public categoryId?: number, public categories?: IProduct[]) {}
}
