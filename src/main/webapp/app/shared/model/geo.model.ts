export interface IGeo {
  id?: number;
  code?: string;
  title?: string;
}

export class Geo implements IGeo {
  constructor(public id?: number, public code?: string, public title?: string) {}
}
