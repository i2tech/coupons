export interface ICurrency {
  id?: number;
  name?: string;
  code?: string;
}

export class Currency implements ICurrency {
  constructor(public id?: number, public name?: string, public code?: string) {}
}
