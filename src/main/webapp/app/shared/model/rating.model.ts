import { Moment } from 'moment';

export interface IRating {
  id?: number;
  rate?: number;
  score?: number;
  ratingTime?: Moment;
  productId?: number;
}

export class Rating implements IRating {
  constructor(public id?: number, public rate?: number, public score?: number, public ratingTime?: Moment, public productId?: number) {}
}
