import { ContactType } from 'app/shared/model/enumerations/contact-type.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface ICustomer {
  id?: number;
  firstName?: string;
  lastName?: string;
  middleName?: string;
  position?: string;
  email?: string;
  mobileNumber?: string;
  faxNumber?: string;
  mailingAddress?: string;
  officeAddress?: string;
  type?: ContactType;
  gender?: Gender;
  active?: boolean;
  myFavotiteId?: number;
  userId?: number;
  historyId?: number;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public middleName?: string,
    public position?: string,
    public email?: string,
    public mobileNumber?: string,
    public faxNumber?: string,
    public mailingAddress?: string,
    public officeAddress?: string,
    public type?: ContactType,
    public gender?: Gender,
    public active?: boolean,
    public myFavotiteId?: number,
    public userId?: number,
    public historyId?: number
  ) {
    this.active = this.active || false;
  }
}
