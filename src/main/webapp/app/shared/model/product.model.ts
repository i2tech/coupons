import { Moment } from 'moment';
import { IPhoto } from 'app/shared/model/photo.model';

export interface IProduct {
  id?: number;
  productId?: number;
  shopOfferId?: number;
  name?: string;
  offerName?: string;
  offerId?: number;
  price?: number;
  priceOld?: number;
  updateDate?: Moment;
  retargeting?: number;
  discount?: number;
  sku?: string;
  upc?: string;
  image?: string;
  shopId?: number;
  orders?: number;
  url?: string;
  similarGoods?: string;
  minPrice?: number;
  maxPrice?: number;
  currencyId?: number;
  photoItems?: IPhoto[];
  myFavoriteId?: number;
  categoryId?: number;
  regionId?: number;
  brandId?: number;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public productId?: number,
    public shopOfferId?: number,
    public name?: string,
    public offerName?: string,
    public offerId?: number,
    public price?: number,
    public priceOld?: number,
    public updateDate?: Moment,
    public retargeting?: number,
    public discount?: number,
    public sku?: string,
    public upc?: string,
    public image?: string,
    public shopId?: number,
    public orders?: number,
    public url?: string,
    public similarGoods?: string,
    public minPrice?: number,
    public maxPrice?: number,
    public currencyId?: number,
    public photoItems?: IPhoto[],
    public myFavoriteId?: number,
    public categoryId?: number,
    public regionId?: number,
    public brandId?: number
  ) {}
}
