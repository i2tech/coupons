import { FileType } from 'app/shared/model/enumerations/file-type.model';

export interface IFile {
  id?: number;
  filename?: string;
  fileOnServer?: string;
  relativePath?: string;
  type?: FileType;
  active?: boolean;
}

export class File implements IFile {
  constructor(
    public id?: number,
    public filename?: string,
    public fileOnServer?: string,
    public relativePath?: string,
    public type?: FileType,
    public active?: boolean
  ) {
    this.active = this.active || false;
  }
}
