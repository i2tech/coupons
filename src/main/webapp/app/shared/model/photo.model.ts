import { IPhotoItem } from 'app/shared/model/photo-item.model';

export interface IPhoto {
  id?: number;
  name?: string;
  active?: boolean;
  photoItems?: IPhotoItem[];
  productId?: number;
  couponId?: number;
}

export class Photo implements IPhoto {
  constructor(
    public id?: number,
    public name?: string,
    public active?: boolean,
    public photoItems?: IPhotoItem[],
    public productId?: number,
    public couponId?: number
  ) {
    this.active = this.active || false;
  }
}
