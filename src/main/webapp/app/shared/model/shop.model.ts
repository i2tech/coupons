import { IProduct } from 'app/shared/model/product.model';

export interface IShop {
  id?: number;
  name?: string;
  code?: string;
  shopId?: number;
  products?: IProduct[];
}

export class Shop implements IShop {
  constructor(public id?: number, public name?: string, public code?: string, public shopId?: number, public products?: IProduct[]) {}
}
