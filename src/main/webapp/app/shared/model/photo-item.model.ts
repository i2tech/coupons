export interface IPhotoItem {
  id?: number;
  item?: string;
  description?: string;
  location?: string;
  priority?: number;
  active?: boolean;
  filesId?: number;
  photoId?: number;
}

export class PhotoItem implements IPhotoItem {
  constructor(
    public id?: number,
    public item?: string,
    public description?: string,
    public location?: string,
    public priority?: number,
    public active?: boolean,
    public filesId?: number,
    public photoId?: number
  ) {
    this.active = this.active || false;
  }
}
