import { IProduct } from 'app/shared/model/product.model';

export interface IRegion {
  id?: number;
  code?: string;
  title?: string;
  products?: IProduct[];
}

export class Region implements IRegion {
  constructor(public id?: number, public code?: string, public title?: string, public products?: IProduct[]) {}
}
