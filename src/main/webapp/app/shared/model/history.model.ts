import { Moment } from 'moment';
import { ICustomer } from 'app/shared/model/customer.model';

export interface IHistory {
  id?: number;
  dateLogin?: Moment;
  ip?: string;
  customers?: ICustomer[];
}

export class History implements IHistory {
  constructor(public id?: number, public dateLogin?: Moment, public ip?: string, public customers?: ICustomer[]) {}
}
