import { Moment } from 'moment';
import { IPhoto } from 'app/shared/model/photo.model';
import { CouponType } from 'app/shared/model/enumerations/coupon-type.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { Look } from 'app/shared/model/enumerations/look.model';

export interface ICoupon {
  id?: number;
  image?: string;
  offerLogo?: string;
  name?: string;
  description?: string;
  startDate?: Moment;
  activeTo?: Moment;
  couponType?: CouponType;
  promoCode?: string;
  offerName?: string;
  offerId?: number;
  status?: Status;
  statusId?: number;
  categoryId?: number;
  actionCategoryName?: string;
  isExclusive?: number;
  isPersonal?: number;
  retargeting?: boolean;
  sectoring?: string;
  rating?: number;
  url?: string;
  urlFrame?: string;
  look?: Look;
  domain?: string;
  geoId?: number;
  photoItems?: IPhoto[];
  myFavoriteId?: number;
}

export class Coupon implements ICoupon {
  constructor(
    public id?: number,
    public image?: string,
    public offerLogo?: string,
    public name?: string,
    public description?: string,
    public startDate?: Moment,
    public activeTo?: Moment,
    public couponType?: CouponType,
    public promoCode?: string,
    public offerName?: string,
    public offerId?: number,
    public status?: Status,
    public statusId?: number,
    public categoryId?: number,
    public actionCategoryName?: string,
    public isExclusive?: number,
    public isPersonal?: number,
    public retargeting?: boolean,
    public sectoring?: string,
    public rating?: number,
    public url?: string,
    public urlFrame?: string,
    public look?: Look,
    public domain?: string,
    public geoId?: number,
    public photoItems?: IPhoto[],
    public myFavoriteId?: number
  ) {
    this.retargeting = this.retargeting || false;
  }
}
