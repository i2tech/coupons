import { ICoupon } from 'app/shared/model/coupon.model';
import { IProduct } from 'app/shared/model/product.model';

export interface IMyFavorite {
  id?: number;
  coupons?: ICoupon[];
  products?: IProduct[];
}

export class MyFavorite implements IMyFavorite {
  constructor(public id?: number, public coupons?: ICoupon[], public products?: IProduct[]) {}
}
