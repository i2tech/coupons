import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from 'app/core/auth/account.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BroadcastSubscribeService } from 'app/broadcast-subscribe.service';

@Component({
  selector: 'jhi-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['sidebar.scss'],
  providers: [NgbDropdownConfig] // add NgbDropdownConfig to the component providers
})
export class SidebarComponent implements OnInit {
  private subscription: Subscription;
  isAdmin = false;
  isSidebarCollapsed = false;
  isHomepage = false;
  currentUrl: any;

  constructor(
    private router: Router,
    protected activatedRoute: ActivatedRoute,
    private accountService: AccountService,
    private config: NgbDropdownConfig,
    private broadcast: BroadcastSubscribeService
  ) {
    // customize default values of dropdowns used by this component tree
    config.autoClose = false;
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  ngOnInit() {
    this.currentUrl = this.router.url;
    /*  this.activatedRoute.data.subscribe(({ isHomepage }) => {
        this.isHomepage = isHomepage;
      });
      this.subscription = this.router.events.subscribe((event) => {
            //if (event instanceof NavigationEnd) {
            if (event instanceof NavigationStart) {
                if (event.url === '/') {
                    this.isHomepage = true;
                }
            }
        });*/
    this.checkUserRole();
  }

  collapseNavbar() {
    this.isSidebarCollapsed = true;
  }

  private checkUserRole() {}
}
