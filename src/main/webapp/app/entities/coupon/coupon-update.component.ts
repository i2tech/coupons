import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ICoupon, Coupon } from 'app/shared/model/coupon.model';
import { CouponService } from './coupon.service';
import { IGeo } from 'app/shared/model/geo.model';
import { GeoService } from 'app/entities/geo/geo.service';
import { IMyFavorite } from 'app/shared/model/my-favorite.model';
import { MyFavoriteService } from 'app/entities/my-favorite/my-favorite.service';

@Component({
  selector: 'jhi-coupon-update',
  templateUrl: './coupon-update.component.html'
})
export class CouponUpdateComponent implements OnInit {
  isSaving: boolean;

  geos: IGeo[];

  myfavorites: IMyFavorite[];

  editForm = this.fb.group({
    id: [],
    image: [],
    offerLogo: [],
    name: [],
    description: [],
    startDate: [],
    activeTo: [],
    couponType: [],
    promoCode: [],
    offerName: [],
    offerId: [],
    status: [],
    statusId: [],
    categoryId: [],
    actionCategoryName: [],
    isExclusive: [],
    isPersonal: [],
    retargeting: [],
    sectoring: [],
    rating: [],
    url: [],
    urlFrame: [],
    look: [],
    domain: [],
    geoId: [],
    myFavoriteId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected couponService: CouponService,
    protected geoService: GeoService,
    protected myFavoriteService: MyFavoriteService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ coupon }) => {
      this.updateForm(coupon);
    });
    this.geoService.query({ filter: 'coupon-is-null' }).subscribe(
      (res: HttpResponse<IGeo[]>) => {
        if (!this.editForm.get('geoId').value) {
          this.geos = res.body;
        } else {
          this.geoService
            .find(this.editForm.get('geoId').value)
            .subscribe(
              (subRes: HttpResponse<IGeo>) => (this.geos = [subRes.body].concat(res.body)),
              (subRes: HttpErrorResponse) => this.onError(subRes.message)
            );
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
    this.myFavoriteService
      .query()
      .subscribe(
        (res: HttpResponse<IMyFavorite[]>) => (this.myfavorites = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(coupon: ICoupon) {
    this.editForm.patchValue({
      id: coupon.id,
      image: coupon.image,
      offerLogo: coupon.offerLogo,
      name: coupon.name,
      description: coupon.description,
      startDate: coupon.startDate != null ? coupon.startDate.format(DATE_TIME_FORMAT) : null,
      activeTo: coupon.activeTo != null ? coupon.activeTo.format(DATE_TIME_FORMAT) : null,
      couponType: coupon.couponType,
      promoCode: coupon.promoCode,
      offerName: coupon.offerName,
      offerId: coupon.offerId,
      status: coupon.status,
      statusId: coupon.statusId,
      categoryId: coupon.categoryId,
      actionCategoryName: coupon.actionCategoryName,
      isExclusive: coupon.isExclusive,
      isPersonal: coupon.isPersonal,
      retargeting: coupon.retargeting,
      sectoring: coupon.sectoring,
      rating: coupon.rating,
      url: coupon.url,
      urlFrame: coupon.urlFrame,
      look: coupon.look,
      domain: coupon.domain,
      geoId: coupon.geoId,
      myFavoriteId: coupon.myFavoriteId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const coupon = this.createFromForm();
    if (coupon.id !== undefined) {
      this.subscribeToSaveResponse(this.couponService.update(coupon));
    } else {
      this.subscribeToSaveResponse(this.couponService.create(coupon));
    }
  }

  private createFromForm(): ICoupon {
    return {
      ...new Coupon(),
      id: this.editForm.get(['id']).value,
      image: this.editForm.get(['image']).value,
      offerLogo: this.editForm.get(['offerLogo']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      startDate:
        this.editForm.get(['startDate']).value != null ? moment(this.editForm.get(['startDate']).value, DATE_TIME_FORMAT) : undefined,
      activeTo: this.editForm.get(['activeTo']).value != null ? moment(this.editForm.get(['activeTo']).value, DATE_TIME_FORMAT) : undefined,
      couponType: this.editForm.get(['couponType']).value,
      promoCode: this.editForm.get(['promoCode']).value,
      offerName: this.editForm.get(['offerName']).value,
      offerId: this.editForm.get(['offerId']).value,
      status: this.editForm.get(['status']).value,
      statusId: this.editForm.get(['statusId']).value,
      categoryId: this.editForm.get(['categoryId']).value,
      actionCategoryName: this.editForm.get(['actionCategoryName']).value,
      isExclusive: this.editForm.get(['isExclusive']).value,
      isPersonal: this.editForm.get(['isPersonal']).value,
      retargeting: this.editForm.get(['retargeting']).value,
      sectoring: this.editForm.get(['sectoring']).value,
      rating: this.editForm.get(['rating']).value,
      url: this.editForm.get(['url']).value,
      urlFrame: this.editForm.get(['urlFrame']).value,
      look: this.editForm.get(['look']).value,
      domain: this.editForm.get(['domain']).value,
      geoId: this.editForm.get(['geoId']).value,
      myFavoriteId: this.editForm.get(['myFavoriteId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoupon>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackGeoById(index: number, item: IGeo) {
    return item.id;
  }

  trackMyFavoriteById(index: number, item: IMyFavorite) {
    return item.id;
  }
}
