import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICoupon } from 'app/shared/model/coupon.model';

type EntityResponseType = HttpResponse<ICoupon>;
type EntityArrayResponseType = HttpResponse<ICoupon[]>;

@Injectable({ providedIn: 'root' })
export class CouponService {
  public resourceUrl = SERVER_API_URL + 'api/coupons';

  constructor(protected http: HttpClient) {}

  create(coupon: ICoupon): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(coupon);
    return this.http
      .post<ICoupon>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(coupon: ICoupon): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(coupon);
    return this.http
      .put<ICoupon>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICoupon>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICoupon[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(coupon: ICoupon): ICoupon {
    const copy: ICoupon = Object.assign({}, coupon, {
      startDate: coupon.startDate != null && coupon.startDate.isValid() ? coupon.startDate.toJSON() : null,
      activeTo: coupon.activeTo != null && coupon.activeTo.isValid() ? coupon.activeTo.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
      res.body.activeTo = res.body.activeTo != null ? moment(res.body.activeTo) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((coupon: ICoupon) => {
        coupon.startDate = coupon.startDate != null ? moment(coupon.startDate) : null;
        coupon.activeTo = coupon.activeTo != null ? moment(coupon.activeTo) : null;
      });
    }
    return res;
  }
}
