import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Coupon } from 'app/shared/model/coupon.model';
import { CouponService } from './coupon.service';
import { CouponComponent } from './coupon.component';
import { CouponDetailComponent } from './coupon-detail.component';
import { CouponUpdateComponent } from './coupon-update.component';
import { ICoupon } from 'app/shared/model/coupon.model';

@Injectable({ providedIn: 'root' })
export class CouponResolve implements Resolve<ICoupon> {
  constructor(private service: CouponService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoupon> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((coupon: HttpResponse<Coupon>) => coupon.body));
    }
    return of(new Coupon());
  }
}

export const couponRoute: Routes = [
  {
    path: '',
    component: CouponComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'couponsApp.coupon.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CouponDetailComponent,
    resolve: {
      coupon: CouponResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.coupon.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CouponUpdateComponent,
    resolve: {
      coupon: CouponResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.coupon.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CouponUpdateComponent,
    resolve: {
      coupon: CouponResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.coupon.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
