import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IHistory, History } from 'app/shared/model/history.model';
import { HistoryService } from './history.service';

@Component({
  selector: 'jhi-history-update',
  templateUrl: './history-update.component.html'
})
export class HistoryUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    dateLogin: [],
    ip: []
  });

  constructor(protected historyService: HistoryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ history }) => {
      this.updateForm(history);
    });
  }

  updateForm(history: IHistory) {
    this.editForm.patchValue({
      id: history.id,
      dateLogin: history.dateLogin != null ? history.dateLogin.format(DATE_TIME_FORMAT) : null,
      ip: history.ip
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const history = this.createFromForm();
    if (history.id !== undefined) {
      this.subscribeToSaveResponse(this.historyService.update(history));
    } else {
      this.subscribeToSaveResponse(this.historyService.create(history));
    }
  }

  private createFromForm(): IHistory {
    return {
      ...new History(),
      id: this.editForm.get(['id']).value,
      dateLogin:
        this.editForm.get(['dateLogin']).value != null ? moment(this.editForm.get(['dateLogin']).value, DATE_TIME_FORMAT) : undefined,
      ip: this.editForm.get(['ip']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHistory>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
