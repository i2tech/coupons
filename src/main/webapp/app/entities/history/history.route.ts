import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { History } from 'app/shared/model/history.model';
import { HistoryService } from './history.service';
import { HistoryComponent } from './history.component';
import { HistoryDetailComponent } from './history-detail.component';
import { HistoryUpdateComponent } from './history-update.component';
import { IHistory } from 'app/shared/model/history.model';

@Injectable({ providedIn: 'root' })
export class HistoryResolve implements Resolve<IHistory> {
  constructor(private service: HistoryService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHistory> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((history: HttpResponse<History>) => history.body));
    }
    return of(new History());
  }
}

export const historyRoute: Routes = [
  {
    path: '',
    component: HistoryComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'couponsApp.history.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HistoryDetailComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.history.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HistoryUpdateComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.history.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HistoryUpdateComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.history.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
