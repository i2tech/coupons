import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHistory } from 'app/shared/model/history.model';
import { HistoryService } from './history.service';

@Component({
  templateUrl: './history-delete-dialog.component.html'
})
export class HistoryDeleteDialogComponent {
  history: IHistory;

  constructor(protected historyService: HistoryService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.historyService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'historyListModification',
        content: 'Deleted an history'
      });
      this.activeModal.dismiss(true);
    });
  }
}
