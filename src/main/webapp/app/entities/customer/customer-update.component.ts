import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { ICustomer, Customer } from 'app/shared/model/customer.model';
import { CustomerService } from './customer.service';
import { IMyFavorite } from 'app/shared/model/my-favorite.model';
import { MyFavoriteService } from 'app/entities/my-favorite/my-favorite.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IHistory } from 'app/shared/model/history.model';
import { HistoryService } from 'app/entities/history/history.service';

@Component({
  selector: 'jhi-customer-update',
  templateUrl: './customer-update.component.html'
})
export class CustomerUpdateComponent implements OnInit {
  isSaving: boolean;

  myfavotites: IMyFavorite[];

  users: IUser[];

  histories: IHistory[];

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: [],
    middleName: [],
    position: [],
    email: [],
    mobileNumber: [],
    faxNumber: [],
    mailingAddress: [],
    officeAddress: [],
    type: [],
    gender: [],
    active: [],
    myFavotiteId: [],
    userId: [],
    historyId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected customerService: CustomerService,
    protected myFavoriteService: MyFavoriteService,
    protected userService: UserService,
    protected historyService: HistoryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ customer }) => {
      this.updateForm(customer);
    });
    this.myFavoriteService.query({ filter: 'customer-is-null' }).subscribe(
      (res: HttpResponse<IMyFavorite[]>) => {
        if (!this.editForm.get('myFavotiteId').value) {
          this.myfavotites = res.body;
        } else {
          this.myFavoriteService
            .find(this.editForm.get('myFavotiteId').value)
            .subscribe(
              (subRes: HttpResponse<IMyFavorite>) => (this.myfavotites = [subRes.body].concat(res.body)),
              (subRes: HttpErrorResponse) => this.onError(subRes.message)
            );
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
    this.userService
      .query()
      .subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.historyService
      .query()
      .subscribe((res: HttpResponse<IHistory[]>) => (this.histories = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(customer: ICustomer) {
    this.editForm.patchValue({
      id: customer.id,
      firstName: customer.firstName,
      lastName: customer.lastName,
      middleName: customer.middleName,
      position: customer.position,
      email: customer.email,
      mobileNumber: customer.mobileNumber,
      faxNumber: customer.faxNumber,
      mailingAddress: customer.mailingAddress,
      officeAddress: customer.officeAddress,
      type: customer.type,
      gender: customer.gender,
      active: customer.active,
      myFavotiteId: customer.myFavotiteId,
      userId: customer.userId,
      historyId: customer.historyId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const customer = this.createFromForm();
    if (customer.id !== undefined) {
      this.subscribeToSaveResponse(this.customerService.update(customer));
    } else {
      this.subscribeToSaveResponse(this.customerService.create(customer));
    }
  }

  private createFromForm(): ICustomer {
    return {
      ...new Customer(),
      id: this.editForm.get(['id']).value,
      firstName: this.editForm.get(['firstName']).value,
      lastName: this.editForm.get(['lastName']).value,
      middleName: this.editForm.get(['middleName']).value,
      position: this.editForm.get(['position']).value,
      email: this.editForm.get(['email']).value,
      mobileNumber: this.editForm.get(['mobileNumber']).value,
      faxNumber: this.editForm.get(['faxNumber']).value,
      mailingAddress: this.editForm.get(['mailingAddress']).value,
      officeAddress: this.editForm.get(['officeAddress']).value,
      type: this.editForm.get(['type']).value,
      gender: this.editForm.get(['gender']).value,
      active: this.editForm.get(['active']).value,
      myFavotiteId: this.editForm.get(['myFavotiteId']).value,
      userId: this.editForm.get(['userId']).value,
      historyId: this.editForm.get(['historyId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomer>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMyFavoriteById(index: number, item: IMyFavorite) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackHistoryById(index: number, item: IHistory) {
    return item.id;
  }
}
