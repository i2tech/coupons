import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'coupon',
        loadChildren: () => import('./coupon/coupon.module').then(m => m.CouponsCouponModule)
      },
      {
        path: 'geo',
        loadChildren: () => import('./geo/geo.module').then(m => m.CouponsGeoModule)
      },
      {
        path: 'file',
        loadChildren: () => import('./file/file.module').then(m => m.CouponsFileModule)
      },
      {
        path: 'photo',
        loadChildren: () => import('./photo/photo.module').then(m => m.CouponsPhotoModule)
      },
      {
        path: 'photo-item',
        loadChildren: () => import('./photo-item/photo-item.module').then(m => m.CouponsPhotoItemModule)
      },
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.CouponsCustomerModule)
      },
      {
        path: 'my-favorite',
        loadChildren: () => import('./my-favorite/my-favorite.module').then(m => m.CouponsMyFavoriteModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.CouponsProductModule)
      },
      {
        path: 'shop',
        loadChildren: () => import('./shop/shop.module').then(m => m.CouponsShopModule)
      },
      {
        path: 'region',
        loadChildren: () => import('./region/region.module').then(m => m.CouponsRegionModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.CouponsCategoryModule)
      },
      {
        path: 'brand',
        loadChildren: () => import('./brand/brand.module').then(m => m.CouponsBrandModule)
      },
      {
        path: 'rating',
        loadChildren: () => import('./rating/rating.module').then(m => m.CouponsRatingModule)
      },
      {
        path: 'currency',
        loadChildren: () => import('./currency/currency.module').then(m => m.CouponsCurrencyModule)
      },
      {
        path: 'history',
        loadChildren: () => import('./history/history.module').then(m => m.CouponsHistoryModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class CouponsEntityModule {}
