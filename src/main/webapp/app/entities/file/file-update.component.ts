import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IFile, File } from 'app/shared/model/file.model';
import { FileService } from './file.service';

@Component({
  selector: 'jhi-file-update',
  templateUrl: './file-update.component.html'
})
export class FileUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    filename: [],
    fileOnServer: [],
    relativePath: [],
    type: [],
    active: []
  });

  constructor(protected fileService: FileService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ file }) => {
      this.updateForm(file);
    });
  }

  updateForm(file: IFile) {
    this.editForm.patchValue({
      id: file.id,
      filename: file.filename,
      fileOnServer: file.fileOnServer,
      relativePath: file.relativePath,
      type: file.type,
      active: file.active
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const file = this.createFromForm();
    if (file.id !== undefined) {
      this.subscribeToSaveResponse(this.fileService.update(file));
    } else {
      this.subscribeToSaveResponse(this.fileService.create(file));
    }
  }

  private createFromForm(): IFile {
    return {
      ...new File(),
      id: this.editForm.get(['id']).value,
      filename: this.editForm.get(['filename']).value,
      fileOnServer: this.editForm.get(['fileOnServer']).value,
      relativePath: this.editForm.get(['relativePath']).value,
      type: this.editForm.get(['type']).value,
      active: this.editForm.get(['active']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFile>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
