import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { File } from 'app/shared/model/file.model';
import { FileService } from './file.service';
import { FileComponent } from './file.component';
import { FileDetailComponent } from './file-detail.component';
import { FileUpdateComponent } from './file-update.component';
import { IFile } from 'app/shared/model/file.model';

@Injectable({ providedIn: 'root' })
export class FileResolve implements Resolve<IFile> {
  constructor(private service: FileService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFile> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((file: HttpResponse<File>) => file.body));
    }
    return of(new File());
  }
}

export const fileRoute: Routes = [
  {
    path: '',
    component: FileComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'couponsApp.file.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FileDetailComponent,
    resolve: {
      file: FileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.file.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FileUpdateComponent,
    resolve: {
      file: FileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.file.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FileUpdateComponent,
    resolve: {
      file: FileResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.file.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
