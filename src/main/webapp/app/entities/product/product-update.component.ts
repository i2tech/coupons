import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IProduct, Product } from 'app/shared/model/product.model';
import { ProductService } from './product.service';
import { ICurrency } from 'app/shared/model/currency.model';
import { CurrencyService } from 'app/entities/currency/currency.service';
import { IMyFavorite } from 'app/shared/model/my-favorite.model';
import { MyFavoriteService } from 'app/entities/my-favorite/my-favorite.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { IShop } from 'app/shared/model/shop.model';
import { ShopService } from 'app/entities/shop/shop.service';
import { IRegion } from 'app/shared/model/region.model';
import { RegionService } from 'app/entities/region/region.service';
import { IBrand } from 'app/shared/model/brand.model';
import { BrandService } from 'app/entities/brand/brand.service';

@Component({
  selector: 'jhi-product-update',
  templateUrl: './product-update.component.html'
})
export class ProductUpdateComponent implements OnInit {
  isSaving: boolean;

  currencies: ICurrency[];

  myfavorites: IMyFavorite[];

  categories: ICategory[];

  shops: IShop[];

  regions: IRegion[];

  brands: IBrand[];

  editForm = this.fb.group({
    id: [],
    productId: [],
    shopOfferId: [],
    name: [],
    offerName: [],
    offerId: [],
    price: [],
    priceOld: [],
    updateDate: [],
    retargeting: [],
    discount: [],
    sku: [],
    upc: [],
    image: [],
    shopId: [],
    orders: [],
    url: [],
    similarGoods: [],
    minPrice: [],
    maxPrice: [],
    currencyId: [],
    myFavoriteId: [],
    categoryId: [],
    regionId: [],
    brandId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected productService: ProductService,
    protected currencyService: CurrencyService,
    protected myFavoriteService: MyFavoriteService,
    protected categoryService: CategoryService,
    protected shopService: ShopService,
    protected regionService: RegionService,
    protected brandService: BrandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ product }) => {
      this.updateForm(product);
    });
    this.currencyService.query({ filter: 'product-is-null' }).subscribe(
      (res: HttpResponse<ICurrency[]>) => {
        if (!this.editForm.get('currencyId').value) {
          this.currencies = res.body;
        } else {
          this.currencyService
            .find(this.editForm.get('currencyId').value)
            .subscribe(
              (subRes: HttpResponse<ICurrency>) => (this.currencies = [subRes.body].concat(res.body)),
              (subRes: HttpErrorResponse) => this.onError(subRes.message)
            );
        }
      },
      (res: HttpErrorResponse) => this.onError(res.message)
    );
    this.myFavoriteService
      .query()
      .subscribe(
        (res: HttpResponse<IMyFavorite[]>) => (this.myfavorites = res.body),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.categoryService
      .query()
      .subscribe((res: HttpResponse<ICategory[]>) => (this.categories = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.shopService
      .query()
      .subscribe((res: HttpResponse<IShop[]>) => (this.shops = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.regionService
      .query()
      .subscribe((res: HttpResponse<IRegion[]>) => (this.regions = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.brandService
      .query()
      .subscribe((res: HttpResponse<IBrand[]>) => (this.brands = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(product: IProduct) {
    this.editForm.patchValue({
      id: product.id,
      productId: product.productId,
      shopOfferId: product.shopOfferId,
      name: product.name,
      offerName: product.offerName,
      offerId: product.offerId,
      price: product.price,
      priceOld: product.priceOld,
      updateDate: product.updateDate != null ? product.updateDate.format(DATE_TIME_FORMAT) : null,
      retargeting: product.retargeting,
      discount: product.discount,
      sku: product.sku,
      upc: product.upc,
      image: product.image,
      shopId: product.shopId,
      orders: product.orders,
      url: product.url,
      similarGoods: product.similarGoods,
      minPrice: product.minPrice,
      maxPrice: product.maxPrice,
      currencyId: product.currencyId,
      myFavoriteId: product.myFavoriteId,
      categoryId: product.categoryId,
      regionId: product.regionId,
      brandId: product.brandId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const product = this.createFromForm();
    if (product.id !== undefined) {
      this.subscribeToSaveResponse(this.productService.update(product));
    } else {
      this.subscribeToSaveResponse(this.productService.create(product));
    }
  }

  private createFromForm(): IProduct {
    return {
      ...new Product(),
      id: this.editForm.get(['id']).value,
      productId: this.editForm.get(['productId']).value,
      shopOfferId: this.editForm.get(['shopOfferId']).value,
      name: this.editForm.get(['name']).value,
      offerName: this.editForm.get(['offerName']).value,
      offerId: this.editForm.get(['offerId']).value,
      price: this.editForm.get(['price']).value,
      priceOld: this.editForm.get(['priceOld']).value,
      updateDate:
        this.editForm.get(['updateDate']).value != null ? moment(this.editForm.get(['updateDate']).value, DATE_TIME_FORMAT) : undefined,
      retargeting: this.editForm.get(['retargeting']).value,
      discount: this.editForm.get(['discount']).value,
      sku: this.editForm.get(['sku']).value,
      upc: this.editForm.get(['upc']).value,
      image: this.editForm.get(['image']).value,
      shopId: this.editForm.get(['shopId']).value,
      orders: this.editForm.get(['orders']).value,
      url: this.editForm.get(['url']).value,
      similarGoods: this.editForm.get(['similarGoods']).value,
      minPrice: this.editForm.get(['minPrice']).value,
      maxPrice: this.editForm.get(['maxPrice']).value,
      currencyId: this.editForm.get(['currencyId']).value,
      myFavoriteId: this.editForm.get(['myFavoriteId']).value,
      categoryId: this.editForm.get(['categoryId']).value,
      regionId: this.editForm.get(['regionId']).value,
      brandId: this.editForm.get(['brandId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCurrencyById(index: number, item: ICurrency) {
    return item.id;
  }

  trackMyFavoriteById(index: number, item: IMyFavorite) {
    return item.id;
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }

  trackShopById(index: number, item: IShop) {
    return item.id;
  }

  trackRegionById(index: number, item: IRegion) {
    return item.id;
  }

  trackBrandById(index: number, item: IBrand) {
    return item.id;
  }
}
