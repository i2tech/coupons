import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IPhoto, Photo } from 'app/shared/model/photo.model';
import { PhotoService } from './photo.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { ICoupon } from 'app/shared/model/coupon.model';
import { CouponService } from 'app/entities/coupon/coupon.service';

@Component({
  selector: 'jhi-photo-update',
  templateUrl: './photo-update.component.html'
})
export class PhotoUpdateComponent implements OnInit {
  isSaving: boolean;

  products: IProduct[];

  coupons: ICoupon[];

  editForm = this.fb.group({
    id: [],
    name: [],
    active: [],
    productId: [],
    couponId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected photoService: PhotoService,
    protected productService: ProductService,
    protected couponService: CouponService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ photo }) => {
      this.updateForm(photo);
    });
    this.productService
      .query()
      .subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.couponService
      .query()
      .subscribe((res: HttpResponse<ICoupon[]>) => (this.coupons = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(photo: IPhoto) {
    this.editForm.patchValue({
      id: photo.id,
      name: photo.name,
      active: photo.active,
      productId: photo.productId,
      couponId: photo.couponId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const photo = this.createFromForm();
    if (photo.id !== undefined) {
      this.subscribeToSaveResponse(this.photoService.update(photo));
    } else {
      this.subscribeToSaveResponse(this.photoService.create(photo));
    }
  }

  private createFromForm(): IPhoto {
    return {
      ...new Photo(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      active: this.editForm.get(['active']).value,
      productId: this.editForm.get(['productId']).value,
      couponId: this.editForm.get(['couponId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhoto>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProductById(index: number, item: IProduct) {
    return item.id;
  }

  trackCouponById(index: number, item: ICoupon) {
    return item.id;
  }
}
