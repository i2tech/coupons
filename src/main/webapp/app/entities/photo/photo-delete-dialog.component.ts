import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPhoto } from 'app/shared/model/photo.model';
import { PhotoService } from './photo.service';

@Component({
  templateUrl: './photo-delete-dialog.component.html'
})
export class PhotoDeleteDialogComponent {
  photo: IPhoto;

  constructor(protected photoService: PhotoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.photoService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'photoListModification',
        content: 'Deleted an photo'
      });
      this.activeModal.dismiss(true);
    });
  }
}
