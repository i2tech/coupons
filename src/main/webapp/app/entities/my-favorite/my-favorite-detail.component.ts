import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMyFavorite } from 'app/shared/model/my-favorite.model';

@Component({
  selector: 'jhi-my-favorite-detail',
  templateUrl: './my-favorite-detail.component.html'
})
export class MyFavoriteDetailComponent implements OnInit {
  myFavorite: IMyFavorite;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ myFavorite }) => {
      this.myFavorite = myFavorite;
    });
  }

  previousState() {
    window.history.back();
  }
}
