import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MyFavorite } from 'app/shared/model/my-favorite.model';
import { MyFavoriteService } from './my-favorite.service';
import { MyFavoriteComponent } from './my-favorite.component';
import { MyFavoriteDetailComponent } from './my-favorite-detail.component';
import { MyFavoriteUpdateComponent } from './my-favorite-update.component';
import { IMyFavorite } from 'app/shared/model/my-favorite.model';

@Injectable({ providedIn: 'root' })
export class MyFavoriteResolve implements Resolve<IMyFavorite> {
  constructor(private service: MyFavoriteService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMyFavorite> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((myFavorite: HttpResponse<MyFavorite>) => myFavorite.body));
    }
    return of(new MyFavorite());
  }
}

export const myFavoriteRoute: Routes = [
  {
    path: '',
    component: MyFavoriteComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'couponsApp.myFavorite.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MyFavoriteDetailComponent,
    resolve: {
      myFavorite: MyFavoriteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.myFavorite.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MyFavoriteUpdateComponent,
    resolve: {
      myFavorite: MyFavoriteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.myFavorite.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MyFavoriteUpdateComponent,
    resolve: {
      myFavorite: MyFavoriteResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.myFavorite.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
