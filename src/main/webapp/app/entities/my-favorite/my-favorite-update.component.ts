import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IMyFavorite, MyFavorite } from 'app/shared/model/my-favorite.model';
import { MyFavoriteService } from './my-favorite.service';

@Component({
  selector: 'jhi-my-favorite-update',
  templateUrl: './my-favorite-update.component.html'
})
export class MyFavoriteUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: []
  });

  constructor(protected myFavoriteService: MyFavoriteService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ myFavorite }) => {
      this.updateForm(myFavorite);
    });
  }

  updateForm(myFavorite: IMyFavorite) {
    this.editForm.patchValue({
      id: myFavorite.id
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const myFavorite = this.createFromForm();
    if (myFavorite.id !== undefined) {
      this.subscribeToSaveResponse(this.myFavoriteService.update(myFavorite));
    } else {
      this.subscribeToSaveResponse(this.myFavoriteService.create(myFavorite));
    }
  }

  private createFromForm(): IMyFavorite {
    return {
      ...new MyFavorite(),
      id: this.editForm.get(['id']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMyFavorite>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
