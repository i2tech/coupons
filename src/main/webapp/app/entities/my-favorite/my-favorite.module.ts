import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CouponsSharedModule } from 'app/shared/shared.module';
import { MyFavoriteComponent } from './my-favorite.component';
import { MyFavoriteDetailComponent } from './my-favorite-detail.component';
import { MyFavoriteUpdateComponent } from './my-favorite-update.component';
import { MyFavoriteDeleteDialogComponent } from './my-favorite-delete-dialog.component';
import { myFavoriteRoute } from './my-favorite.route';

@NgModule({
  imports: [CouponsSharedModule, RouterModule.forChild(myFavoriteRoute)],
  declarations: [MyFavoriteComponent, MyFavoriteDetailComponent, MyFavoriteUpdateComponent, MyFavoriteDeleteDialogComponent],
  entryComponents: [MyFavoriteDeleteDialogComponent]
})
export class CouponsMyFavoriteModule {}
