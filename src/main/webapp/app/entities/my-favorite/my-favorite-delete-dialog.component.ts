import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMyFavorite } from 'app/shared/model/my-favorite.model';
import { MyFavoriteService } from './my-favorite.service';

@Component({
  templateUrl: './my-favorite-delete-dialog.component.html'
})
export class MyFavoriteDeleteDialogComponent {
  myFavorite: IMyFavorite;

  constructor(
    protected myFavoriteService: MyFavoriteService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.myFavoriteService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'myFavoriteListModification',
        content: 'Deleted an myFavorite'
      });
      this.activeModal.dismiss(true);
    });
  }
}
