import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMyFavorite } from 'app/shared/model/my-favorite.model';

type EntityResponseType = HttpResponse<IMyFavorite>;
type EntityArrayResponseType = HttpResponse<IMyFavorite[]>;

@Injectable({ providedIn: 'root' })
export class MyFavoriteService {
  public resourceUrl = SERVER_API_URL + 'api/my-favorites';

  constructor(protected http: HttpClient) {}

  create(myFavorite: IMyFavorite): Observable<EntityResponseType> {
    return this.http.post<IMyFavorite>(this.resourceUrl, myFavorite, { observe: 'response' });
  }

  update(myFavorite: IMyFavorite): Observable<EntityResponseType> {
    return this.http.put<IMyFavorite>(this.resourceUrl, myFavorite, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMyFavorite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMyFavorite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
