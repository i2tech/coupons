import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPhotoItem } from 'app/shared/model/photo-item.model';
import { PhotoItemService } from './photo-item.service';

@Component({
  templateUrl: './photo-item-delete-dialog.component.html'
})
export class PhotoItemDeleteDialogComponent {
  photoItem: IPhotoItem;

  constructor(protected photoItemService: PhotoItemService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.photoItemService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'photoItemListModification',
        content: 'Deleted an photoItem'
      });
      this.activeModal.dismiss(true);
    });
  }
}
