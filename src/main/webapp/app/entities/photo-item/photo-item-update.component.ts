import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';
import { IPhotoItem, PhotoItem } from 'app/shared/model/photo-item.model';
import { PhotoItemService } from './photo-item.service';
import { IFile } from 'app/shared/model/file.model';
import { FileService } from 'app/entities/file/file.service';
import { IPhoto } from 'app/shared/model/photo.model';
import { PhotoService } from 'app/entities/photo/photo.service';

@Component({
  selector: 'jhi-photo-item-update',
  templateUrl: './photo-item-update.component.html'
})
export class PhotoItemUpdateComponent implements OnInit {
  isSaving: boolean;

  files: IFile[];

  photos: IPhoto[];

  editForm = this.fb.group({
    id: [],
    item: [],
    description: [],
    location: [],
    priority: [],
    active: [],
    filesId: [],
    photoId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected photoItemService: PhotoItemService,
    protected fileService: FileService,
    protected photoService: PhotoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ photoItem }) => {
      this.updateForm(photoItem);
    });
    this.fileService
      .query()
      .subscribe((res: HttpResponse<IFile[]>) => (this.files = res.body), (res: HttpErrorResponse) => this.onError(res.message));
    this.photoService
      .query()
      .subscribe((res: HttpResponse<IPhoto[]>) => (this.photos = res.body), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(photoItem: IPhotoItem) {
    this.editForm.patchValue({
      id: photoItem.id,
      item: photoItem.item,
      description: photoItem.description,
      location: photoItem.location,
      priority: photoItem.priority,
      active: photoItem.active,
      filesId: photoItem.filesId,
      photoId: photoItem.photoId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const photoItem = this.createFromForm();
    if (photoItem.id !== undefined) {
      this.subscribeToSaveResponse(this.photoItemService.update(photoItem));
    } else {
      this.subscribeToSaveResponse(this.photoItemService.create(photoItem));
    }
  }

  private createFromForm(): IPhotoItem {
    return {
      ...new PhotoItem(),
      id: this.editForm.get(['id']).value,
      item: this.editForm.get(['item']).value,
      description: this.editForm.get(['description']).value,
      location: this.editForm.get(['location']).value,
      priority: this.editForm.get(['priority']).value,
      active: this.editForm.get(['active']).value,
      filesId: this.editForm.get(['filesId']).value,
      photoId: this.editForm.get(['photoId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPhotoItem>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackFileById(index: number, item: IFile) {
    return item.id;
  }

  trackPhotoById(index: number, item: IPhoto) {
    return item.id;
  }
}
