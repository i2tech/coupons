import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CouponsSharedModule } from 'app/shared/shared.module';
import { PhotoItemComponent } from './photo-item.component';
import { PhotoItemDetailComponent } from './photo-item-detail.component';
import { PhotoItemUpdateComponent } from './photo-item-update.component';
import { PhotoItemDeleteDialogComponent } from './photo-item-delete-dialog.component';
import { photoItemRoute } from './photo-item.route';

@NgModule({
  imports: [CouponsSharedModule, RouterModule.forChild(photoItemRoute)],
  declarations: [PhotoItemComponent, PhotoItemDetailComponent, PhotoItemUpdateComponent, PhotoItemDeleteDialogComponent],
  entryComponents: [PhotoItemDeleteDialogComponent]
})
export class CouponsPhotoItemModule {}
