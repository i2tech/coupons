import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPhotoItem } from 'app/shared/model/photo-item.model';

@Component({
  selector: 'jhi-photo-item-detail',
  templateUrl: './photo-item-detail.component.html'
})
export class PhotoItemDetailComponent implements OnInit {
  photoItem: IPhotoItem;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ photoItem }) => {
      this.photoItem = photoItem;
    });
  }

  previousState() {
    window.history.back();
  }
}
