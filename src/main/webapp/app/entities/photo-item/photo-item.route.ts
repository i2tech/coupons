import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PhotoItem } from 'app/shared/model/photo-item.model';
import { PhotoItemService } from './photo-item.service';
import { PhotoItemComponent } from './photo-item.component';
import { PhotoItemDetailComponent } from './photo-item-detail.component';
import { PhotoItemUpdateComponent } from './photo-item-update.component';
import { IPhotoItem } from 'app/shared/model/photo-item.model';

@Injectable({ providedIn: 'root' })
export class PhotoItemResolve implements Resolve<IPhotoItem> {
  constructor(private service: PhotoItemService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPhotoItem> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((photoItem: HttpResponse<PhotoItem>) => photoItem.body));
    }
    return of(new PhotoItem());
  }
}

export const photoItemRoute: Routes = [
  {
    path: '',
    component: PhotoItemComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'couponsApp.photoItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PhotoItemDetailComponent,
    resolve: {
      photoItem: PhotoItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.photoItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PhotoItemUpdateComponent,
    resolve: {
      photoItem: PhotoItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.photoItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PhotoItemUpdateComponent,
    resolve: {
      photoItem: PhotoItemResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.photoItem.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
