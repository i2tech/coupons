import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IGeo } from 'app/shared/model/geo.model';
import { GeoService } from './geo.service';

@Component({
  templateUrl: './geo-delete-dialog.component.html'
})
export class GeoDeleteDialogComponent {
  geo: IGeo;

  constructor(protected geoService: GeoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.geoService.delete(id).subscribe(() => {
      this.eventManager.broadcast({
        name: 'geoListModification',
        content: 'Deleted an geo'
      });
      this.activeModal.dismiss(true);
    });
  }
}
