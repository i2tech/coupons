import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IGeo } from 'app/shared/model/geo.model';

@Component({
  selector: 'jhi-geo-detail',
  templateUrl: './geo-detail.component.html'
})
export class GeoDetailComponent implements OnInit {
  geo: IGeo;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ geo }) => {
      this.geo = geo;
    });
  }

  previousState() {
    window.history.back();
  }
}
