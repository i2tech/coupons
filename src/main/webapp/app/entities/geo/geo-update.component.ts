import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IGeo, Geo } from 'app/shared/model/geo.model';
import { GeoService } from './geo.service';

@Component({
  selector: 'jhi-geo-update',
  templateUrl: './geo-update.component.html'
})
export class GeoUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    code: [],
    title: []
  });

  constructor(protected geoService: GeoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ geo }) => {
      this.updateForm(geo);
    });
  }

  updateForm(geo: IGeo) {
    this.editForm.patchValue({
      id: geo.id,
      code: geo.code,
      title: geo.title
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const geo = this.createFromForm();
    if (geo.id !== undefined) {
      this.subscribeToSaveResponse(this.geoService.update(geo));
    } else {
      this.subscribeToSaveResponse(this.geoService.create(geo));
    }
  }

  private createFromForm(): IGeo {
    return {
      ...new Geo(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      title: this.editForm.get(['title']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IGeo>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
