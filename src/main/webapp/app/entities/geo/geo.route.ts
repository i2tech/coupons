import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Geo } from 'app/shared/model/geo.model';
import { GeoService } from './geo.service';
import { GeoComponent } from './geo.component';
import { GeoDetailComponent } from './geo-detail.component';
import { GeoUpdateComponent } from './geo-update.component';
import { IGeo } from 'app/shared/model/geo.model';

@Injectable({ providedIn: 'root' })
export class GeoResolve implements Resolve<IGeo> {
  constructor(private service: GeoService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IGeo> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(map((geo: HttpResponse<Geo>) => geo.body));
    }
    return of(new Geo());
  }
}

export const geoRoute: Routes = [
  {
    path: '',
    component: GeoComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'couponsApp.geo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: GeoDetailComponent,
    resolve: {
      geo: GeoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.geo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: GeoUpdateComponent,
    resolve: {
      geo: GeoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.geo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: GeoUpdateComponent,
    resolve: {
      geo: GeoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'couponsApp.geo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
