import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CouponsSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { SidebarComponent } from 'app/layouts/navbar/sidebar/sidebar.component';

@NgModule({
  imports: [CouponsSharedModule, RouterModule.forChild([HOME_ROUTE])],
  exports: [SidebarComponent],
  declarations: [HomeComponent, SidebarComponent]
})
export class CouponsHomeModule {}
