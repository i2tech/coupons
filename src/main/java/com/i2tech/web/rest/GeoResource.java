package com.i2tech.web.rest;

import com.i2tech.service.GeoService;
import com.i2tech.web.rest.errors.BadRequestAlertException;
import com.i2tech.service.dto.GeoDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.i2tech.domain.Geo}.
 */
@RestController
@RequestMapping("/api")
public class GeoResource {

    private final Logger log = LoggerFactory.getLogger(GeoResource.class);

    private static final String ENTITY_NAME = "geo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GeoService geoService;

    public GeoResource(GeoService geoService) {
        this.geoService = geoService;
    }

    /**
     * {@code POST  /geos} : Create a new geo.
     *
     * @param geoDTO the geoDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new geoDTO, or with status {@code 400 (Bad Request)} if the geo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/geos")
    public ResponseEntity<GeoDTO> createGeo(@RequestBody GeoDTO geoDTO) throws URISyntaxException {
        log.debug("REST request to save Geo : {}", geoDTO);
        if (geoDTO.getId() != null) {
            throw new BadRequestAlertException("A new geo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GeoDTO result = geoService.save(geoDTO);
        return ResponseEntity.created(new URI("/api/geos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /geos} : Updates an existing geo.
     *
     * @param geoDTO the geoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated geoDTO,
     * or with status {@code 400 (Bad Request)} if the geoDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the geoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/geos")
    public ResponseEntity<GeoDTO> updateGeo(@RequestBody GeoDTO geoDTO) throws URISyntaxException {
        log.debug("REST request to update Geo : {}", geoDTO);
        if (geoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GeoDTO result = geoService.save(geoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, geoDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /geos} : get all the geos.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of geos in body.
     */
    @GetMapping("/geos")
    public ResponseEntity<List<GeoDTO>> getAllGeos(Pageable pageable) {
        log.debug("REST request to get a page of Geos");
        Page<GeoDTO> page = geoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /geos/:id} : get the "id" geo.
     *
     * @param id the id of the geoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the geoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/geos/{id}")
    public ResponseEntity<GeoDTO> getGeo(@PathVariable Long id) {
        log.debug("REST request to get Geo : {}", id);
        Optional<GeoDTO> geoDTO = geoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(geoDTO);
    }

    /**
     * {@code DELETE  /geos/:id} : delete the "id" geo.
     *
     * @param id the id of the geoDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/geos/{id}")
    public ResponseEntity<Void> deleteGeo(@PathVariable Long id) {
        log.debug("REST request to delete Geo : {}", id);
        geoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
