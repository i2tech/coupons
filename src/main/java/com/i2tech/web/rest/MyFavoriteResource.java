package com.i2tech.web.rest;

import com.i2tech.service.MyFavoriteService;
import com.i2tech.web.rest.errors.BadRequestAlertException;
import com.i2tech.service.dto.MyFavoriteDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.i2tech.domain.MyFavorite}.
 */
@RestController
@RequestMapping("/api")
public class MyFavoriteResource {

    private final Logger log = LoggerFactory.getLogger(MyFavoriteResource.class);

    private static final String ENTITY_NAME = "myFavorite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MyFavoriteService myFavoriteService;

    public MyFavoriteResource(MyFavoriteService myFavoriteService) {
        this.myFavoriteService = myFavoriteService;
    }

    /**
     * {@code POST  /my-favorites} : Create a new myFavorite.
     *
     * @param myFavoriteDTO the myFavoriteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new myFavoriteDTO, or with status {@code 400 (Bad Request)} if the myFavorite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/my-favorites")
    public ResponseEntity<MyFavoriteDTO> createMyFavorite(@RequestBody MyFavoriteDTO myFavoriteDTO) throws URISyntaxException {
        log.debug("REST request to save MyFavorite : {}", myFavoriteDTO);
        if (myFavoriteDTO.getId() != null) {
            throw new BadRequestAlertException("A new myFavorite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MyFavoriteDTO result = myFavoriteService.save(myFavoriteDTO);
        return ResponseEntity.created(new URI("/api/my-favorites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /my-favorites} : Updates an existing myFavorite.
     *
     * @param myFavoriteDTO the myFavoriteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated myFavoriteDTO,
     * or with status {@code 400 (Bad Request)} if the myFavoriteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the myFavoriteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/my-favorites")
    public ResponseEntity<MyFavoriteDTO> updateMyFavorite(@RequestBody MyFavoriteDTO myFavoriteDTO) throws URISyntaxException {
        log.debug("REST request to update MyFavorite : {}", myFavoriteDTO);
        if (myFavoriteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MyFavoriteDTO result = myFavoriteService.save(myFavoriteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, myFavoriteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /my-favorites} : get all the myFavorites.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of myFavorites in body.
     */
    @GetMapping("/my-favorites")
    public ResponseEntity<List<MyFavoriteDTO>> getAllMyFavorites(Pageable pageable) {
        log.debug("REST request to get a page of MyFavorites");
        Page<MyFavoriteDTO> page = myFavoriteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /my-favorites/:id} : get the "id" myFavorite.
     *
     * @param id the id of the myFavoriteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the myFavoriteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/my-favorites/{id}")
    public ResponseEntity<MyFavoriteDTO> getMyFavorite(@PathVariable Long id) {
        log.debug("REST request to get MyFavorite : {}", id);
        Optional<MyFavoriteDTO> myFavoriteDTO = myFavoriteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(myFavoriteDTO);
    }

    /**
     * {@code DELETE  /my-favorites/:id} : delete the "id" myFavorite.
     *
     * @param id the id of the myFavoriteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/my-favorites/{id}")
    public ResponseEntity<Void> deleteMyFavorite(@PathVariable Long id) {
        log.debug("REST request to delete MyFavorite : {}", id);
        myFavoriteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
