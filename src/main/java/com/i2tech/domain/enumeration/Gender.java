package com.i2tech.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE, FEMALE, OTHER
}
