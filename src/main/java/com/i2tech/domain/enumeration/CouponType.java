package com.i2tech.domain.enumeration;

/**
 * The CouponType enumeration.
 */
public enum CouponType {
    SALE, PROMO_CODES, FREE_SHIPPING, EXTRAS_WITH_ORDER, SPECIAL_EVENT
}
