package com.i2tech.domain.enumeration;

/**
 * The FileType enumeration.
 */
public enum FileType {
    PHOTO, LOGO, DOCUMENT
}
