package com.i2tech.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    ACTIVE, DEACTIVE
}
