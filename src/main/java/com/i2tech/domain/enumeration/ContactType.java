package com.i2tech.domain.enumeration;

/**
 * The ContactType enumeration.
 */
public enum ContactType {
    DEVELOPER, CUSTOMER, ADMIN
}
