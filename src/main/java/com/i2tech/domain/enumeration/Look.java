package com.i2tech.domain.enumeration;

/**
 * The Look enumeration.
 */
public enum Look {
    ONLINE, OFFLINE
}
