package com.i2tech.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import com.i2tech.domain.enumeration.CouponType;

import com.i2tech.domain.enumeration.Status;

import com.i2tech.domain.enumeration.Look;

/**
 * A Coupon.
 */
@Entity
@Table(name = "coupon")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Coupon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "image")
    private String image;

    @Column(name = "offer_logo")
    private String offerLogo;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "active_to")
    private ZonedDateTime activeTo;

    @Enumerated(EnumType.STRING)
    @Column(name = "coupon_type")
    private CouponType couponType;

    @Column(name = "promo_code")
    private String promoCode;

    @Column(name = "offer_name")
    private String offerName;

    @Column(name = "offer_id")
    private Long offerId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "status_id")
    private Long statusId;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "action_category_name")
    private String actionCategoryName;

    @Column(name = "is_exclusive")
    private Integer isExclusive;

    @Column(name = "is_personal")
    private Integer isPersonal;

    @Column(name = "retargeting")
    private Boolean retargeting;

    @Column(name = "sectoring")
    private String sectoring;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "url")
    private String url;

    @Column(name = "url_frame")
    private String urlFrame;

    @Enumerated(EnumType.STRING)
    @Column(name = "look")
    private Look look;

    @Column(name = "domain")
    private String domain;

    @OneToOne
    @JoinColumn(unique = true)
    private Geo geo;

    @OneToMany(mappedBy = "coupon")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Photo> photoItems = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("coupons")
    private MyFavorite myFavorite;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public Coupon image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOfferLogo() {
        return offerLogo;
    }

    public Coupon offerLogo(String offerLogo) {
        this.offerLogo = offerLogo;
        return this;
    }

    public void setOfferLogo(String offerLogo) {
        this.offerLogo = offerLogo;
    }

    public String getName() {
        return name;
    }

    public Coupon name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Coupon description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public Coupon startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getActiveTo() {
        return activeTo;
    }

    public Coupon activeTo(ZonedDateTime activeTo) {
        this.activeTo = activeTo;
        return this;
    }

    public void setActiveTo(ZonedDateTime activeTo) {
        this.activeTo = activeTo;
    }

    public CouponType getCouponType() {
        return couponType;
    }

    public Coupon couponType(CouponType couponType) {
        this.couponType = couponType;
        return this;
    }

    public void setCouponType(CouponType couponType) {
        this.couponType = couponType;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public Coupon promoCode(String promoCode) {
        this.promoCode = promoCode;
        return this;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getOfferName() {
        return offerName;
    }

    public Coupon offerName(String offerName) {
        this.offerName = offerName;
        return this;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getOfferId() {
        return offerId;
    }

    public Coupon offerId(Long offerId) {
        this.offerId = offerId;
        return this;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Status getStatus() {
        return status;
    }

    public Coupon status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStatusId() {
        return statusId;
    }

    public Coupon statusId(Long statusId) {
        this.statusId = statusId;
        return this;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Coupon categoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getActionCategoryName() {
        return actionCategoryName;
    }

    public Coupon actionCategoryName(String actionCategoryName) {
        this.actionCategoryName = actionCategoryName;
        return this;
    }

    public void setActionCategoryName(String actionCategoryName) {
        this.actionCategoryName = actionCategoryName;
    }

    public Integer getIsExclusive() {
        return isExclusive;
    }

    public Coupon isExclusive(Integer isExclusive) {
        this.isExclusive = isExclusive;
        return this;
    }

    public void setIsExclusive(Integer isExclusive) {
        this.isExclusive = isExclusive;
    }

    public Integer getIsPersonal() {
        return isPersonal;
    }

    public Coupon isPersonal(Integer isPersonal) {
        this.isPersonal = isPersonal;
        return this;
    }

    public void setIsPersonal(Integer isPersonal) {
        this.isPersonal = isPersonal;
    }

    public Boolean isRetargeting() {
        return retargeting;
    }

    public Coupon retargeting(Boolean retargeting) {
        this.retargeting = retargeting;
        return this;
    }

    public void setRetargeting(Boolean retargeting) {
        this.retargeting = retargeting;
    }

    public String getSectoring() {
        return sectoring;
    }

    public Coupon sectoring(String sectoring) {
        this.sectoring = sectoring;
        return this;
    }

    public void setSectoring(String sectoring) {
        this.sectoring = sectoring;
    }

    public Integer getRating() {
        return rating;
    }

    public Coupon rating(Integer rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public Coupon url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlFrame() {
        return urlFrame;
    }

    public Coupon urlFrame(String urlFrame) {
        this.urlFrame = urlFrame;
        return this;
    }

    public void setUrlFrame(String urlFrame) {
        this.urlFrame = urlFrame;
    }

    public Look getLook() {
        return look;
    }

    public Coupon look(Look look) {
        this.look = look;
        return this;
    }

    public void setLook(Look look) {
        this.look = look;
    }

    public String getDomain() {
        return domain;
    }

    public Coupon domain(String domain) {
        this.domain = domain;
        return this;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Geo getGeo() {
        return geo;
    }

    public Coupon geo(Geo geo) {
        this.geo = geo;
        return this;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public Set<Photo> getPhotoItems() {
        return photoItems;
    }

    public Coupon photoItems(Set<Photo> photos) {
        this.photoItems = photos;
        return this;
    }

    public Coupon addPhotoItems(Photo photo) {
        this.photoItems.add(photo);
        photo.setCoupon(this);
        return this;
    }

    public Coupon removePhotoItems(Photo photo) {
        this.photoItems.remove(photo);
        photo.setCoupon(null);
        return this;
    }

    public void setPhotoItems(Set<Photo> photos) {
        this.photoItems = photos;
    }

    public MyFavorite getMyFavorite() {
        return myFavorite;
    }

    public Coupon myFavorite(MyFavorite myFavorite) {
        this.myFavorite = myFavorite;
        return this;
    }

    public void setMyFavorite(MyFavorite myFavorite) {
        this.myFavorite = myFavorite;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coupon)) {
            return false;
        }
        return id != null && id.equals(((Coupon) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Coupon{" +
            "id=" + getId() +
            ", image='" + getImage() + "'" +
            ", offerLogo='" + getOfferLogo() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", activeTo='" + getActiveTo() + "'" +
            ", couponType='" + getCouponType() + "'" +
            ", promoCode='" + getPromoCode() + "'" +
            ", offerName='" + getOfferName() + "'" +
            ", offerId=" + getOfferId() +
            ", status='" + getStatus() + "'" +
            ", statusId=" + getStatusId() +
            ", categoryId=" + getCategoryId() +
            ", actionCategoryName='" + getActionCategoryName() + "'" +
            ", isExclusive=" + getIsExclusive() +
            ", isPersonal=" + getIsPersonal() +
            ", retargeting='" + isRetargeting() + "'" +
            ", sectoring='" + getSectoring() + "'" +
            ", rating=" + getRating() +
            ", url='" + getUrl() + "'" +
            ", urlFrame='" + getUrlFrame() + "'" +
            ", look='" + getLook() + "'" +
            ", domain='" + getDomain() + "'" +
            "}";
    }
}
