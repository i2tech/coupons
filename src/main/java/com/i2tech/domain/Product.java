package com.i2tech.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "shop_offer_id")
    private Long shopOfferId;

    @Column(name = "name")
    private String name;

    @Column(name = "offer_name")
    private String offerName;

    @Column(name = "offer_id")
    private Long offerId;

    @Column(name = "price")
    private Double price;

    @Column(name = "price_old")
    private Double priceOld;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @Column(name = "retargeting")
    private Integer retargeting;

    @Column(name = "discount")
    private Double discount;

    @Column(name = "sku")
    private String sku;

    @Column(name = "upc")
    private String upc;

    @Column(name = "image")
    private String image;

    @Column(name = "orders")
    private Integer orders;

    @Column(name = "url")
    private String url;

    @Column(name = "similar_goods")
    private String similarGoods;

    @Column(name = "min_price")
    private Integer minPrice;

    @Column(name = "max_price")
    private Integer maxPrice;

    @OneToOne
    @JoinColumn(unique = true)
    private Currency currency;

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Photo> photoItems = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("products")
    private MyFavorite myFavorite;

    @ManyToOne
    @JsonIgnoreProperties("categories")
    private Category category;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Shop shop;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Region region;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Brand brand;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public Product productId(Long productId) {
        this.productId = productId;
        return this;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getShopOfferId() {
        return shopOfferId;
    }

    public Product shopOfferId(Long shopOfferId) {
        this.shopOfferId = shopOfferId;
        return this;
    }

    public void setShopOfferId(Long shopOfferId) {
        this.shopOfferId = shopOfferId;
    }

    public String getName() {
        return name;
    }

    public Product name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfferName() {
        return offerName;
    }

    public Product offerName(String offerName) {
        this.offerName = offerName;
        return this;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getOfferId() {
        return offerId;
    }

    public Product offerId(Long offerId) {
        this.offerId = offerId;
        return this;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Double getPrice() {
        return price;
    }

    public Product price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceOld() {
        return priceOld;
    }

    public Product priceOld(Double priceOld) {
        this.priceOld = priceOld;
        return this;
    }

    public void setPriceOld(Double priceOld) {
        this.priceOld = priceOld;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public Product updateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getRetargeting() {
        return retargeting;
    }

    public Product retargeting(Integer retargeting) {
        this.retargeting = retargeting;
        return this;
    }

    public void setRetargeting(Integer retargeting) {
        this.retargeting = retargeting;
    }

    public Double getDiscount() {
        return discount;
    }

    public Product discount(Double discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getSku() {
        return sku;
    }

    public Product sku(String sku) {
        this.sku = sku;
        return this;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUpc() {
        return upc;
    }

    public Product upc(String upc) {
        this.upc = upc;
        return this;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getImage() {
        return image;
    }

    public Product image(String image) {
        this.image = image;
        return this;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getOrders() {
        return orders;
    }

    public Product orders(Integer orders) {
        this.orders = orders;
        return this;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public String getUrl() {
        return url;
    }

    public Product url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSimilarGoods() {
        return similarGoods;
    }

    public Product similarGoods(String similarGoods) {
        this.similarGoods = similarGoods;
        return this;
    }

    public void setSimilarGoods(String similarGoods) {
        this.similarGoods = similarGoods;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public Product minPrice(Integer minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public Product maxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Product currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Set<Photo> getPhotoItems() {
        return photoItems;
    }

    public Product photoItems(Set<Photo> photos) {
        this.photoItems = photos;
        return this;
    }

    public Product addPhotoItems(Photo photo) {
        this.photoItems.add(photo);
        photo.setProduct(this);
        return this;
    }

    public Product removePhotoItems(Photo photo) {
        this.photoItems.remove(photo);
        photo.setProduct(null);
        return this;
    }

    public void setPhotoItems(Set<Photo> photos) {
        this.photoItems = photos;
    }

    public MyFavorite getMyFavorite() {
        return myFavorite;
    }

    public Product myFavorite(MyFavorite myFavorite) {
        this.myFavorite = myFavorite;
        return this;
    }

    public void setMyFavorite(MyFavorite myFavorite) {
        this.myFavorite = myFavorite;
    }

    public Category getCategory() {
        return category;
    }

    public Product category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Shop getShop() {
        return shop;
    }

    public Product shop(Shop shop) {
        this.shop = shop;
        return this;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Region getRegion() {
        return region;
    }

    public Product region(Region region) {
        this.region = region;
        return this;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Brand getBrand() {
        return brand;
    }

    public Product brand(Brand brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", productId=" + getProductId() +
            ", shopOfferId=" + getShopOfferId() +
            ", name='" + getName() + "'" +
            ", offerName='" + getOfferName() + "'" +
            ", offerId=" + getOfferId() +
            ", price=" + getPrice() +
            ", priceOld=" + getPriceOld() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", retargeting=" + getRetargeting() +
            ", discount=" + getDiscount() +
            ", sku='" + getSku() + "'" +
            ", upc='" + getUpc() + "'" +
            ", image='" + getImage() + "'" +
            ", orders=" + getOrders() +
            ", url='" + getUrl() + "'" +
            ", similarGoods='" + getSimilarGoods() + "'" +
            ", minPrice=" + getMinPrice() +
            ", maxPrice=" + getMaxPrice() +
            "}";
    }
}
