package com.i2tech.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A History.
 */
@Entity
@Table(name = "history")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class History implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_login")
    private ZonedDateTime dateLogin;

    @Column(name = "ip")
    private String ip;

    @OneToMany(mappedBy = "history")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Customer> customers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateLogin() {
        return dateLogin;
    }

    public History dateLogin(ZonedDateTime dateLogin) {
        this.dateLogin = dateLogin;
        return this;
    }

    public void setDateLogin(ZonedDateTime dateLogin) {
        this.dateLogin = dateLogin;
    }

    public String getIp() {
        return ip;
    }

    public History ip(String ip) {
        this.ip = ip;
        return this;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public History customers(Set<Customer> customers) {
        this.customers = customers;
        return this;
    }

    public History addCustomer(Customer customer) {
        this.customers.add(customer);
        customer.setHistory(this);
        return this;
    }

    public History removeCustomer(Customer customer) {
        this.customers.remove(customer);
        customer.setHistory(null);
        return this;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof History)) {
            return false;
        }
        return id != null && id.equals(((History) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "History{" +
            "id=" + getId() +
            ", dateLogin='" + getDateLogin() + "'" +
            ", ip='" + getIp() + "'" +
            "}";
    }
}
