package com.i2tech.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Photo.
 */
@Entity
@Table(name = "photo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Photo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private Boolean active;

    @OneToMany(mappedBy = "photo")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PhotoItem> photoItems = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("photoItems")
    private Product product;

    @ManyToOne
    @JsonIgnoreProperties("photoItems")
    private Coupon coupon;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Photo name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isActive() {
        return active;
    }

    public Photo active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Set<PhotoItem> getPhotoItems() {
        return photoItems;
    }

    public Photo photoItems(Set<PhotoItem> photoItems) {
        this.photoItems = photoItems;
        return this;
    }

    public Photo addPhotoItems(PhotoItem photoItem) {
        this.photoItems.add(photoItem);
        photoItem.setPhoto(this);
        return this;
    }

    public Photo removePhotoItems(PhotoItem photoItem) {
        this.photoItems.remove(photoItem);
        photoItem.setPhoto(null);
        return this;
    }

    public void setPhotoItems(Set<PhotoItem> photoItems) {
        this.photoItems = photoItems;
    }

    public Product getProduct() {
        return product;
    }

    public Photo product(Product product) {
        this.product = product;
        return this;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public Photo coupon(Coupon coupon) {
        this.coupon = coupon;
        return this;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Photo)) {
            return false;
        }
        return id != null && id.equals(((Photo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Photo{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
