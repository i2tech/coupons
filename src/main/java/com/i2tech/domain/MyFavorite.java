package com.i2tech.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A MyFavorite.
 */
@Entity
@Table(name = "my_favorite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MyFavorite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "myFavorite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Coupon> coupons = new HashSet<>();

    @OneToMany(mappedBy = "myFavorite")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Product> products = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Coupon> getCoupons() {
        return coupons;
    }

    public MyFavorite coupons(Set<Coupon> coupons) {
        this.coupons = coupons;
        return this;
    }

    public MyFavorite addCoupons(Coupon coupon) {
        this.coupons.add(coupon);
        coupon.setMyFavorite(this);
        return this;
    }

    public MyFavorite removeCoupons(Coupon coupon) {
        this.coupons.remove(coupon);
        coupon.setMyFavorite(null);
        return this;
    }

    public void setCoupons(Set<Coupon> coupons) {
        this.coupons = coupons;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public MyFavorite products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public MyFavorite addProducts(Product product) {
        this.products.add(product);
        product.setMyFavorite(this);
        return this;
    }

    public MyFavorite removeProducts(Product product) {
        this.products.remove(product);
        product.setMyFavorite(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MyFavorite)) {
            return false;
        }
        return id != null && id.equals(((MyFavorite) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MyFavorite{" +
            "id=" + getId() +
            "}";
    }
}
