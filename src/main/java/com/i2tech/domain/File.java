package com.i2tech.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

import com.i2tech.domain.enumeration.FileType;

/**
 * A File.
 */
@Entity
@Table(name = "file")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class File implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "filename")
    private String filename;

    @Column(name = "file_on_server")
    private String fileOnServer;

    @Column(name = "relative_path")
    private String relativePath;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private FileType type;

    @Column(name = "active")
    private Boolean active;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public File filename(String filename) {
        this.filename = filename;
        return this;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileOnServer() {
        return fileOnServer;
    }

    public File fileOnServer(String fileOnServer) {
        this.fileOnServer = fileOnServer;
        return this;
    }

    public void setFileOnServer(String fileOnServer) {
        this.fileOnServer = fileOnServer;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public File relativePath(String relativePath) {
        this.relativePath = relativePath;
        return this;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public FileType getType() {
        return type;
    }

    public File type(FileType type) {
        this.type = type;
        return this;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public Boolean isActive() {
        return active;
    }

    public File active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof File)) {
            return false;
        }
        return id != null && id.equals(((File) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "File{" +
            "id=" + getId() +
            ", filename='" + getFilename() + "'" +
            ", fileOnServer='" + getFileOnServer() + "'" +
            ", relativePath='" + getRelativePath() + "'" +
            ", type='" + getType() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
