package com.i2tech.repository;
import com.i2tech.domain.PhotoItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PhotoItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PhotoItemRepository extends JpaRepository<PhotoItem, Long> {

}
