package com.i2tech.repository;
import com.i2tech.domain.MyFavorite;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MyFavorite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MyFavoriteRepository extends JpaRepository<MyFavorite, Long> {

}
