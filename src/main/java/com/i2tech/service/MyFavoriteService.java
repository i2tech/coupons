package com.i2tech.service;

import com.i2tech.service.dto.MyFavoriteDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.i2tech.domain.MyFavorite}.
 */
public interface MyFavoriteService {

    /**
     * Save a myFavorite.
     *
     * @param myFavoriteDTO the entity to save.
     * @return the persisted entity.
     */
    MyFavoriteDTO save(MyFavoriteDTO myFavoriteDTO);

    /**
     * Get all the myFavorites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MyFavoriteDTO> findAll(Pageable pageable);


    /**
     * Get the "id" myFavorite.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MyFavoriteDTO> findOne(Long id);

    /**
     * Delete the "id" myFavorite.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
