package com.i2tech.service;

import com.i2tech.service.dto.PhotoItemDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.i2tech.domain.PhotoItem}.
 */
public interface PhotoItemService {

    /**
     * Save a photoItem.
     *
     * @param photoItemDTO the entity to save.
     * @return the persisted entity.
     */
    PhotoItemDTO save(PhotoItemDTO photoItemDTO);

    /**
     * Get all the photoItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PhotoItemDTO> findAll(Pageable pageable);


    /**
     * Get the "id" photoItem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PhotoItemDTO> findOne(Long id);

    /**
     * Delete the "id" photoItem.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
