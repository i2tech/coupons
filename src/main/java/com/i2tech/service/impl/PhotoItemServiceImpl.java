package com.i2tech.service.impl;

import com.i2tech.service.PhotoItemService;
import com.i2tech.domain.PhotoItem;
import com.i2tech.repository.PhotoItemRepository;
import com.i2tech.service.dto.PhotoItemDTO;
import com.i2tech.service.mapper.PhotoItemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PhotoItem}.
 */
@Service
@Transactional
public class PhotoItemServiceImpl implements PhotoItemService {

    private final Logger log = LoggerFactory.getLogger(PhotoItemServiceImpl.class);

    private final PhotoItemRepository photoItemRepository;

    private final PhotoItemMapper photoItemMapper;

    public PhotoItemServiceImpl(PhotoItemRepository photoItemRepository, PhotoItemMapper photoItemMapper) {
        this.photoItemRepository = photoItemRepository;
        this.photoItemMapper = photoItemMapper;
    }

    /**
     * Save a photoItem.
     *
     * @param photoItemDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public PhotoItemDTO save(PhotoItemDTO photoItemDTO) {
        log.debug("Request to save PhotoItem : {}", photoItemDTO);
        PhotoItem photoItem = photoItemMapper.toEntity(photoItemDTO);
        photoItem = photoItemRepository.save(photoItem);
        return photoItemMapper.toDto(photoItem);
    }

    /**
     * Get all the photoItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PhotoItemDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PhotoItems");
        return photoItemRepository.findAll(pageable)
            .map(photoItemMapper::toDto);
    }


    /**
     * Get one photoItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PhotoItemDTO> findOne(Long id) {
        log.debug("Request to get PhotoItem : {}", id);
        return photoItemRepository.findById(id)
            .map(photoItemMapper::toDto);
    }

    /**
     * Delete the photoItem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PhotoItem : {}", id);
        photoItemRepository.deleteById(id);
    }
}
