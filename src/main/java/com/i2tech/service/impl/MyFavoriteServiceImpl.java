package com.i2tech.service.impl;

import com.i2tech.service.MyFavoriteService;
import com.i2tech.domain.MyFavorite;
import com.i2tech.repository.MyFavoriteRepository;
import com.i2tech.service.dto.MyFavoriteDTO;
import com.i2tech.service.mapper.MyFavoriteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MyFavorite}.
 */
@Service
@Transactional
public class MyFavoriteServiceImpl implements MyFavoriteService {

    private final Logger log = LoggerFactory.getLogger(MyFavoriteServiceImpl.class);

    private final MyFavoriteRepository myFavoriteRepository;

    private final MyFavoriteMapper myFavoriteMapper;

    public MyFavoriteServiceImpl(MyFavoriteRepository myFavoriteRepository, MyFavoriteMapper myFavoriteMapper) {
        this.myFavoriteRepository = myFavoriteRepository;
        this.myFavoriteMapper = myFavoriteMapper;
    }

    /**
     * Save a myFavorite.
     *
     * @param myFavoriteDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MyFavoriteDTO save(MyFavoriteDTO myFavoriteDTO) {
        log.debug("Request to save MyFavorite : {}", myFavoriteDTO);
        MyFavorite myFavorite = myFavoriteMapper.toEntity(myFavoriteDTO);
        myFavorite = myFavoriteRepository.save(myFavorite);
        return myFavoriteMapper.toDto(myFavorite);
    }

    /**
     * Get all the myFavorites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MyFavoriteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MyFavorites");
        return myFavoriteRepository.findAll(pageable)
            .map(myFavoriteMapper::toDto);
    }


    /**
     * Get one myFavorite by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MyFavoriteDTO> findOne(Long id) {
        log.debug("Request to get MyFavorite : {}", id);
        return myFavoriteRepository.findById(id)
            .map(myFavoriteMapper::toDto);
    }

    /**
     * Delete the myFavorite by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MyFavorite : {}", id);
        myFavoriteRepository.deleteById(id);
    }
}
