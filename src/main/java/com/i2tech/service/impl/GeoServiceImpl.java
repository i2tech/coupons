package com.i2tech.service.impl;

import com.i2tech.service.GeoService;
import com.i2tech.domain.Geo;
import com.i2tech.repository.GeoRepository;
import com.i2tech.service.dto.GeoDTO;
import com.i2tech.service.mapper.GeoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Geo}.
 */
@Service
@Transactional
public class GeoServiceImpl implements GeoService {

    private final Logger log = LoggerFactory.getLogger(GeoServiceImpl.class);

    private final GeoRepository geoRepository;

    private final GeoMapper geoMapper;

    public GeoServiceImpl(GeoRepository geoRepository, GeoMapper geoMapper) {
        this.geoRepository = geoRepository;
        this.geoMapper = geoMapper;
    }

    /**
     * Save a geo.
     *
     * @param geoDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public GeoDTO save(GeoDTO geoDTO) {
        log.debug("Request to save Geo : {}", geoDTO);
        Geo geo = geoMapper.toEntity(geoDTO);
        geo = geoRepository.save(geo);
        return geoMapper.toDto(geo);
    }

    /**
     * Get all the geos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<GeoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Geos");
        return geoRepository.findAll(pageable)
            .map(geoMapper::toDto);
    }


    /**
     * Get one geo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<GeoDTO> findOne(Long id) {
        log.debug("Request to get Geo : {}", id);
        return geoRepository.findById(id)
            .map(geoMapper::toDto);
    }

    /**
     * Delete the geo by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Geo : {}", id);
        geoRepository.deleteById(id);
    }
}
