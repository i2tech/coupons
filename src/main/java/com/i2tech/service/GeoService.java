package com.i2tech.service;

import com.i2tech.service.dto.GeoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.i2tech.domain.Geo}.
 */
public interface GeoService {

    /**
     * Save a geo.
     *
     * @param geoDTO the entity to save.
     * @return the persisted entity.
     */
    GeoDTO save(GeoDTO geoDTO);

    /**
     * Get all the geos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<GeoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" geo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GeoDTO> findOne(Long id);

    /**
     * Delete the "id" geo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
