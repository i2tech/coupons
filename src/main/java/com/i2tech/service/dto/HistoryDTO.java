package com.i2tech.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.i2tech.domain.History} entity.
 */
public class HistoryDTO implements Serializable {

    private Long id;

    private ZonedDateTime dateLogin;

    private String ip;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateLogin() {
        return dateLogin;
    }

    public void setDateLogin(ZonedDateTime dateLogin) {
        this.dateLogin = dateLogin;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HistoryDTO historyDTO = (HistoryDTO) o;
        if (historyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), historyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HistoryDTO{" +
            "id=" + getId() +
            ", dateLogin='" + getDateLogin() + "'" +
            ", ip='" + getIp() + "'" +
            "}";
    }
}
