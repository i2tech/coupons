package com.i2tech.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import com.i2tech.domain.enumeration.CouponType;
import com.i2tech.domain.enumeration.Status;
import com.i2tech.domain.enumeration.Look;

/**
 * A DTO for the {@link com.i2tech.domain.Coupon} entity.
 */
public class CouponDTO implements Serializable {

    private Long id;

    private String image;

    private String offerLogo;

    private String name;

    private String description;

    private ZonedDateTime startDate;

    private ZonedDateTime activeTo;

    private CouponType couponType;

    private String promoCode;

    private String offerName;

    private Long offerId;

    private Status status;

    private Long statusId;

    private Long categoryId;

    private String actionCategoryName;

    private Integer isExclusive;

    private Integer isPersonal;

    private Boolean retargeting;

    private String sectoring;

    private Integer rating;

    private String url;

    private String urlFrame;

    private Look look;

    private String domain;


    private Long geoId;

    private Long myFavoriteId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOfferLogo() {
        return offerLogo;
    }

    public void setOfferLogo(String offerLogo) {
        this.offerLogo = offerLogo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getActiveTo() {
        return activeTo;
    }

    public void setActiveTo(ZonedDateTime activeTo) {
        this.activeTo = activeTo;
    }

    public CouponType getCouponType() {
        return couponType;
    }

    public void setCouponType(CouponType couponType) {
        this.couponType = couponType;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getActionCategoryName() {
        return actionCategoryName;
    }

    public void setActionCategoryName(String actionCategoryName) {
        this.actionCategoryName = actionCategoryName;
    }

    public Integer getIsExclusive() {
        return isExclusive;
    }

    public void setIsExclusive(Integer isExclusive) {
        this.isExclusive = isExclusive;
    }

    public Integer getIsPersonal() {
        return isPersonal;
    }

    public void setIsPersonal(Integer isPersonal) {
        this.isPersonal = isPersonal;
    }

    public Boolean isRetargeting() {
        return retargeting;
    }

    public void setRetargeting(Boolean retargeting) {
        this.retargeting = retargeting;
    }

    public String getSectoring() {
        return sectoring;
    }

    public void setSectoring(String sectoring) {
        this.sectoring = sectoring;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlFrame() {
        return urlFrame;
    }

    public void setUrlFrame(String urlFrame) {
        this.urlFrame = urlFrame;
    }

    public Look getLook() {
        return look;
    }

    public void setLook(Look look) {
        this.look = look;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Long getGeoId() {
        return geoId;
    }

    public void setGeoId(Long geoId) {
        this.geoId = geoId;
    }

    public Long getMyFavoriteId() {
        return myFavoriteId;
    }

    public void setMyFavoriteId(Long myFavoriteId) {
        this.myFavoriteId = myFavoriteId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CouponDTO couponDTO = (CouponDTO) o;
        if (couponDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), couponDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CouponDTO{" +
            "id=" + getId() +
            ", image='" + getImage() + "'" +
            ", offerLogo='" + getOfferLogo() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", activeTo='" + getActiveTo() + "'" +
            ", couponType='" + getCouponType() + "'" +
            ", promoCode='" + getPromoCode() + "'" +
            ", offerName='" + getOfferName() + "'" +
            ", offerId=" + getOfferId() +
            ", status='" + getStatus() + "'" +
            ", statusId=" + getStatusId() +
            ", categoryId=" + getCategoryId() +
            ", actionCategoryName='" + getActionCategoryName() + "'" +
            ", isExclusive=" + getIsExclusive() +
            ", isPersonal=" + getIsPersonal() +
            ", retargeting='" + isRetargeting() + "'" +
            ", sectoring='" + getSectoring() + "'" +
            ", rating=" + getRating() +
            ", url='" + getUrl() + "'" +
            ", urlFrame='" + getUrlFrame() + "'" +
            ", look='" + getLook() + "'" +
            ", domain='" + getDomain() + "'" +
            ", geo=" + getGeoId() +
            ", myFavorite=" + getMyFavoriteId() +
            "}";
    }
}
