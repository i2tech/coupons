package com.i2tech.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.i2tech.domain.MyFavorite} entity.
 */
public class MyFavoriteDTO implements Serializable {

    private Long id;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MyFavoriteDTO myFavoriteDTO = (MyFavoriteDTO) o;
        if (myFavoriteDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), myFavoriteDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MyFavoriteDTO{" +
            "id=" + getId() +
            "}";
    }
}
