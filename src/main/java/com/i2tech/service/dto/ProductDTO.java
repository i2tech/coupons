package com.i2tech.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.i2tech.domain.Product} entity.
 */
public class ProductDTO implements Serializable {

    private Long id;

    private Long productId;

    private Long shopOfferId;

    private String name;

    private String offerName;

    private Long offerId;

    private Double price;

    private Double priceOld;

    private ZonedDateTime updateDate;

    private Integer retargeting;

    private Double discount;

    private String sku;

    private String upc;

    private String image;

    private Long shopId;

    private Integer orders;

    private String url;

    private String similarGoods;

    private Integer minPrice;

    private Integer maxPrice;


    private Long currencyId;

    private Long myFavoriteId;

    private Long categoryId;

    private Long regionId;

    private Long brandId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getShopOfferId() {
        return shopOfferId;
    }

    public void setShopOfferId(Long shopOfferId) {
        this.shopOfferId = shopOfferId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(Double priceOld) {
        this.priceOld = priceOld;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getRetargeting() {
        return retargeting;
    }

    public void setRetargeting(Integer retargeting) {
        this.retargeting = retargeting;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSimilarGoods() {
        return similarGoods;
    }

    public void setSimilarGoods(String similarGoods) {
        this.similarGoods = similarGoods;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Integer minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Integer maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Long getMyFavoriteId() {
        return myFavoriteId;
    }

    public void setMyFavoriteId(Long myFavoriteId) {
        this.myFavoriteId = myFavoriteId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductDTO productDTO = (ProductDTO) o;
        if (productDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + getId() +
            ", productId=" + getProductId() +
            ", shopOfferId=" + getShopOfferId() +
            ", name='" + getName() + "'" +
            ", offerName='" + getOfferName() + "'" +
            ", offerId=" + getOfferId() +
            ", price=" + getPrice() +
            ", priceOld=" + getPriceOld() +
            ", updateDate='" + getUpdateDate() + "'" +
            ", retargeting=" + getRetargeting() +
            ", discount=" + getDiscount() +
            ", sku='" + getSku() + "'" +
            ", upc='" + getUpc() + "'" +
            ", image='" + getImage() + "'" +
            ", shopId=" + getShopId() +
            ", orders=" + getOrders() +
            ", url='" + getUrl() + "'" +
            ", similarGoods='" + getSimilarGoods() + "'" +
            ", minPrice=" + getMinPrice() +
            ", maxPrice=" + getMaxPrice() +
            ", currency=" + getCurrencyId() +
            ", myFavorite=" + getMyFavoriteId() +
            ", category=" + getCategoryId() +
            ", shop=" + getShopId() +
            ", region=" + getRegionId() +
            ", brand=" + getBrandId() +
            "}";
    }
}
