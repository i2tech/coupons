package com.i2tech.service.dto;
import java.io.Serializable;
import java.util.Objects;
import com.i2tech.domain.enumeration.FileType;

/**
 * A DTO for the {@link com.i2tech.domain.File} entity.
 */
public class FileDTO implements Serializable {

    private Long id;

    private String filename;

    private String fileOnServer;

    private String relativePath;

    private FileType type;

    private Boolean active;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFileOnServer() {
        return fileOnServer;
    }

    public void setFileOnServer(String fileOnServer) {
        this.fileOnServer = fileOnServer;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FileDTO fileDTO = (FileDTO) o;
        if (fileDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), fileDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FileDTO{" +
            "id=" + getId() +
            ", filename='" + getFilename() + "'" +
            ", fileOnServer='" + getFileOnServer() + "'" +
            ", relativePath='" + getRelativePath() + "'" +
            ", type='" + getType() + "'" +
            ", active='" + isActive() + "'" +
            "}";
    }
}
