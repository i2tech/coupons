package com.i2tech.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.i2tech.domain.PhotoItem} entity.
 */
public class PhotoItemDTO implements Serializable {

    private Long id;

    private String item;

    private String description;

    private String location;

    private Integer priority;

    private Boolean active;


    private Long filesId;

    private Long photoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getFilesId() {
        return filesId;
    }

    public void setFilesId(Long fileId) {
        this.filesId = fileId;
    }

    public Long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Long photoId) {
        this.photoId = photoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PhotoItemDTO photoItemDTO = (PhotoItemDTO) o;
        if (photoItemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), photoItemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhotoItemDTO{" +
            "id=" + getId() +
            ", item='" + getItem() + "'" +
            ", description='" + getDescription() + "'" +
            ", location='" + getLocation() + "'" +
            ", priority=" + getPriority() +
            ", active='" + isActive() + "'" +
            ", files=" + getFilesId() +
            ", photo=" + getPhotoId() +
            "}";
    }
}
