package com.i2tech.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.i2tech.domain.Geo} entity.
 */
public class GeoDTO implements Serializable {

    private Long id;

    private String code;

    private String title;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GeoDTO geoDTO = (GeoDTO) o;
        if (geoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), geoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GeoDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
