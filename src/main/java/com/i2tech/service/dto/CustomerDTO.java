package com.i2tech.service.dto;
import java.io.Serializable;
import java.util.Objects;
import com.i2tech.domain.enumeration.ContactType;
import com.i2tech.domain.enumeration.Gender;

/**
 * A DTO for the {@link com.i2tech.domain.Customer} entity.
 */
public class CustomerDTO implements Serializable {

    private Long id;

    private String firstName;

    private String lastName;

    private String middleName;

    private String position;

    private String email;

    private String mobileNumber;

    private String faxNumber;

    private String mailingAddress;

    private String officeAddress;

    private ContactType type;

    private Gender gender;

    private Boolean active;


    private Long myFavotiteId;

    private Long userId;

    private Long historyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getMyFavotiteId() {
        return myFavotiteId;
    }

    public void setMyFavotiteId(Long myFavoriteId) {
        this.myFavotiteId = myFavoriteId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomerDTO customerDTO = (CustomerDTO) o;
        if (customerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", position='" + getPosition() + "'" +
            ", email='" + getEmail() + "'" +
            ", mobileNumber='" + getMobileNumber() + "'" +
            ", faxNumber='" + getFaxNumber() + "'" +
            ", mailingAddress='" + getMailingAddress() + "'" +
            ", officeAddress='" + getOfficeAddress() + "'" +
            ", type='" + getType() + "'" +
            ", gender='" + getGender() + "'" +
            ", active='" + isActive() + "'" +
            ", myFavotite=" + getMyFavotiteId() +
            ", user=" + getUserId() +
            ", history=" + getHistoryId() +
            "}";
    }
}
