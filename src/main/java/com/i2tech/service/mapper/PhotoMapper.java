package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.PhotoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Photo} and its DTO {@link PhotoDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, CouponMapper.class})
public interface PhotoMapper extends EntityMapper<PhotoDTO, Photo> {

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "coupon.id", target = "couponId")
    PhotoDTO toDto(Photo photo);

    @Mapping(target = "photoItems", ignore = true)
    @Mapping(target = "removePhotoItems", ignore = true)
    @Mapping(source = "productId", target = "product")
    @Mapping(source = "couponId", target = "coupon")
    Photo toEntity(PhotoDTO photoDTO);

    default Photo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Photo photo = new Photo();
        photo.setId(id);
        return photo;
    }
}
