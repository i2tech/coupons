package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.CouponDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Coupon} and its DTO {@link CouponDTO}.
 */
@Mapper(componentModel = "spring", uses = {GeoMapper.class, MyFavoriteMapper.class})
public interface CouponMapper extends EntityMapper<CouponDTO, Coupon> {

    @Mapping(source = "geo.id", target = "geoId")
    @Mapping(source = "myFavorite.id", target = "myFavoriteId")
    CouponDTO toDto(Coupon coupon);

    @Mapping(source = "geoId", target = "geo")
    @Mapping(target = "photoItems", ignore = true)
    @Mapping(target = "removePhotoItems", ignore = true)
    @Mapping(source = "myFavoriteId", target = "myFavorite")
    Coupon toEntity(CouponDTO couponDTO);

    default Coupon fromId(Long id) {
        if (id == null) {
            return null;
        }
        Coupon coupon = new Coupon();
        coupon.setId(id);
        return coupon;
    }
}
