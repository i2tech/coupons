package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.CustomerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Customer} and its DTO {@link CustomerDTO}.
 */
@Mapper(componentModel = "spring", uses = {MyFavoriteMapper.class, UserMapper.class, HistoryMapper.class})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(source = "myFavotite.id", target = "myFavotiteId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "history.id", target = "historyId")
    CustomerDTO toDto(Customer customer);

    @Mapping(source = "myFavotiteId", target = "myFavotite")
    @Mapping(source = "userId", target = "user")
    @Mapping(source = "historyId", target = "history")
    Customer toEntity(CustomerDTO customerDTO);

    default Customer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }
}
