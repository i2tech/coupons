package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.MyFavoriteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MyFavorite} and its DTO {@link MyFavoriteDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MyFavoriteMapper extends EntityMapper<MyFavoriteDTO, MyFavorite> {


    @Mapping(target = "coupons", ignore = true)
    @Mapping(target = "removeCoupons", ignore = true)
    @Mapping(target = "products", ignore = true)
    @Mapping(target = "removeProducts", ignore = true)
    MyFavorite toEntity(MyFavoriteDTO myFavoriteDTO);

    default MyFavorite fromId(Long id) {
        if (id == null) {
            return null;
        }
        MyFavorite myFavorite = new MyFavorite();
        myFavorite.setId(id);
        return myFavorite;
    }
}
