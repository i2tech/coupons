package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.ProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = {CurrencyMapper.class, MyFavoriteMapper.class, CategoryMapper.class, ShopMapper.class, RegionMapper.class, BrandMapper.class})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(source = "currency.id", target = "currencyId")
    @Mapping(source = "myFavorite.id", target = "myFavoriteId")
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "shop.id", target = "shopId")
    @Mapping(source = "region.id", target = "regionId")
    @Mapping(source = "brand.id", target = "brandId")
    ProductDTO toDto(Product product);

    @Mapping(source = "currencyId", target = "currency")
    @Mapping(target = "photoItems", ignore = true)
    @Mapping(target = "removePhotoItems", ignore = true)
    @Mapping(source = "myFavoriteId", target = "myFavorite")
    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "shopId", target = "shop")
    @Mapping(source = "regionId", target = "region")
    @Mapping(source = "brandId", target = "brand")
    Product toEntity(ProductDTO productDTO);

    default Product fromId(Long id) {
        if (id == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        return product;
    }
}
