package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.GeoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Geo} and its DTO {@link GeoDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GeoMapper extends EntityMapper<GeoDTO, Geo> {



    default Geo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Geo geo = new Geo();
        geo.setId(id);
        return geo;
    }
}
