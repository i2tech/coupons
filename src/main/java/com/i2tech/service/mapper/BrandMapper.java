package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.BrandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Brand} and its DTO {@link BrandDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BrandMapper extends EntityMapper<BrandDTO, Brand> {


    @Mapping(target = "products", ignore = true)
    @Mapping(target = "removeProducts", ignore = true)
    Brand toEntity(BrandDTO brandDTO);

    default Brand fromId(Long id) {
        if (id == null) {
            return null;
        }
        Brand brand = new Brand();
        brand.setId(id);
        return brand;
    }
}
