package com.i2tech.service.mapper;

import com.i2tech.domain.*;
import com.i2tech.service.dto.PhotoItemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PhotoItem} and its DTO {@link PhotoItemDTO}.
 */
@Mapper(componentModel = "spring", uses = {FileMapper.class, PhotoMapper.class})
public interface PhotoItemMapper extends EntityMapper<PhotoItemDTO, PhotoItem> {

    @Mapping(source = "files.id", target = "filesId")
    @Mapping(source = "photo.id", target = "photoId")
    PhotoItemDTO toDto(PhotoItem photoItem);

    @Mapping(source = "filesId", target = "files")
    @Mapping(source = "photoId", target = "photo")
    PhotoItem toEntity(PhotoItemDTO photoItemDTO);

    default PhotoItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        PhotoItem photoItem = new PhotoItem();
        photoItem.setId(id);
        return photoItem;
    }
}
